import java.io.File;
import java.util.Map;
import java.util.TreeMap;

public class JarDiffFinder {
    public static void main(String[] args){
        String src = args[0];
        String dst = args[1];
        boolean printCommonJarsWithSameVersionAlso = false;
        if(args.length > 2){
            printCommonJarsWithSameVersionAlso = Boolean.parseBoolean(args[2]);
        }
        Map<String, String> srcMap = new TreeMap<>();
        Map<String, String> dstMap = new TreeMap<>();
        populateMap(src, srcMap);
        populateMap(dst, dstMap);
        Map<String, String> sameJarSameVersion = new TreeMap<>();
        Map<String, String> sameJarDiffVersion = new TreeMap<>();

        String[] jarNames = srcMap.keySet().toArray(new String[srcMap.keySet().size()]);
        for(String jarName : jarNames){
            if(dstMap.containsKey(jarName)){
                if(srcMap.get(jarName).equals(dstMap.get(jarName))){
                    sameJarSameVersion.put(jarName, srcMap.get(jarName));
                    srcMap.remove(jarName);
                    dstMap.remove(jarName);
                }else{
                    sameJarDiffVersion.put(jarName, srcMap.get(jarName) + "," + dstMap.get(jarName));
                    srcMap.remove(jarName);
                    dstMap.remove(jarName);
                }
            }
        }

        if(printCommonJarsWithSameVersionAlso) {
            printSameJarSameVersion(sameJarSameVersion);
        }
        printSameJarDifferentVersion(sameJarDiffVersion);
        printNewlyAddedJars(dstMap);
        printDeletedJars(srcMap);
    }

    public static void printSameJarSameVersion(Map<String, String> map){
        System.out.println("Jar name and version are same in both source and destination");
        System.out.println("========================================================");
        System.out.println("Jar name                                  |       Version");
        System.out.println("========================================================");
        for (String s : map.keySet()) {
            String jarName = addSpace(s,50);
            System.out.println(jarName + map.get(s));
        }
        System.out.println();
        System.out.println();
        System.out.println();
    }

    public static void printSameJarDifferentVersion(Map<String, String> map){
        System.out.println("Jar name same but version is different in source and destination");
        System.out.println("==========================================================================");
        System.out.println("Jar name                                  | Src Version       | Dst Version ");
        System.out.println("==========================================================================");
        for (String s : map.keySet()) {
            String jarName = addSpace(s, 45);
            String version = map.get(s);
            String version1 = version.substring(0, version.indexOf(','));
            String version2 = version.substring(version.indexOf(',') + 1);
            System.out.println(jarName + addSpace(version1, 20) + version2);
        }
        System.out.println();
        System.out.println();
        System.out.println();
    }


    public static void printNewlyAddedJars(Map<String, String> map){
        System.out.println("Newly added jars in destination");
        System.out.println("==========================================================================");
        System.out.println("Jar name                                  | Version");
        System.out.println("==========================================================================");
        for (String s : map.keySet()) {
            String jarName = addSpace(s, 45);
            String version = map.get(s);
            System.out.println(jarName + version);
        }
        System.out.println();
        System.out.println();
        System.out.println();
    }

    public static void printDeletedJars(Map<String, String> map){
        System.out.println("Deleted jars in destination");
        System.out.println("==========================================================================");
        System.out.println("Jar name                                  | Version");
        System.out.println("==========================================================================");
        for (String s : map.keySet()) {
            String jarName = addSpace(s, 45);
            String version = map.get(s);
            System.out.println(jarName + version);
        }
        System.out.println();
        System.out.println();
        System.out.println();
    }

    private static String addSpace(String key, int length){
        StringBuilder sb = new StringBuilder(key);
        if(key.length() < length){
            int num = length - key.length();
            while(num>0){
                num--;
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    public static void populateMap(String path, Map<String, String> map){
        File[] fileList = new File(path).listFiles();
        for(File file: fileList){
            if(file.isDirectory()){
                populateMap(file.getAbsolutePath(), map);
            }else if(file.getName().endsWith("-SNAPSHOT.jar")){
                String fileNameWithVersion = file.getName().substring(0, file.getName().indexOf("-SNAPSHOT.jar"));
                int lastHyphenIndex = fileNameWithVersion.lastIndexOf('-');
                String fileName = fileNameWithVersion;
                String version = "";
                if (lastHyphenIndex > 0) {
                    fileName = fileNameWithVersion.substring(0, lastHyphenIndex);
                    version = fileNameWithVersion.substring(lastHyphenIndex + 1, fileNameWithVersion.length());
                }
                map.put(fileName, version + "-SNAPSHOT");
            }else if(file.getName().endsWith(".jar")){
                String fileNameWithVersion = file.getName().substring(0, file.getName().indexOf(".jar"));
                int lastHyphenIndex = fileNameWithVersion.lastIndexOf('-');
                String fileName = fileNameWithVersion;
                String version = "";
                if (lastHyphenIndex > 0) {
                    fileName = fileNameWithVersion.substring(0, lastHyphenIndex);
                    version = fileNameWithVersion.substring(lastHyphenIndex + 1, fileNameWithVersion.length());
                }
                map.put(fileName, version);
            }
        }
    }
}
