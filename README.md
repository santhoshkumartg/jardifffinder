# JarDiffFinder

If any service is upgraded to latest version, it will add up few more jars to the service. To find the difference between the jars before upgrade and after upgrade this repo will be useful
This program takes three parameters...
1. Path to libraries of the service jar before upgrade
2. Path to libraries of the service jar after upgrade
3. Print the common libraries also (true or false)